package com.example.jsontojsonpropertyobject;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
// todo
public class Test05 {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        String json = "{\"info_list\": [{\"name_s\":\"mkyong\", \"age_s\":37}, {\"name_s\":\"fong\", \"age_s\":38}], \"age\":\"37\", \"pro2\": {p1: \"1111\", p2: 123}}";

        try {

            List<Map<String, Object>> listObject = new ArrayList<>();
            listObject.add(mapper.readValue(json, Map.class));

            while (listObject.size() != 0) {
                Map<String, Object> map = listObject.get(0);
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    System.out.println(entry.getKey() + "/" + entry.getValue());
                    if (entry.getValue() instanceof List) { // array
                        String jsonTemp = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(((List)entry.getValue()).get(0));
                        listObject.add(mapper.readValue(jsonTemp, Map.class));
                    } else if (entry.getValue().toString().startsWith("{")) { // object
                        String jsonTemp = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entry.getValue());
                        listObject.add(mapper.readValue(jsonTemp, Map.class));
                    }
                }
                listObject.remove(0);
                System.out.println("======End object====== ");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
