package com.example.jsontojsonpropertyobject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsontojsonpropertyobjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsontojsonpropertyobjectApplication.class, args);
	}

}
